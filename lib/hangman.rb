require 'byebug'
class Hangman
  attr_reader :guesser, :referee, :board
  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @board = []
  end

  def setup
    @secret_length = @referee.pick_secret_word
    @guesser.register_secret_length(@secret_length)
    @board = [nil] * @secret_length
  end

  def take_turn
  letter =  @guesser.guess(board)
  indices = @referee.check_guess(letter)
  update_board(letter, indices)
    @guess.handle_response(letter, indices)
  end
end

class HumanPlayer
  def initialize
    @guesssed_letters = []
  end

  def register_secret_length(num)
    puts "The word #{num} length."
  end

  def guess(board)
  end

  def check_guess
  end

  def handle_response
  end
end

class ComputerPlayer
  def initialize(dictionary)
    @dictionary = dictionary
  end

  def candidate_words
    @dictionary
  end

  def pick_secret_word
    @word = @dictionary.sample
    @word.length
  end

  def check_guess(letter)
    indices = []
    @word.chars.each_with_index do |ch, idx|
      indices << idx if ch == letter
    end
    indices
  end

  def register_secret_length(num)
    candidate_words.select! {|el| el.length == num}
  end

  def guess(board)
    alphabet = ("a".."z").to_a
    new_hash = Hash.new(0)
    @dictionary.each do |word|
      word.each_char do |ch|
        new_hash[ch] += 1
      end
    end
    most = new_hash.max_by {|k,v| v}.first
    if @board.nil?
      most
    else
    delete_most = new_hash.reject {|k,v| k == most}
    delete_most.max_by {|k,v| v}.first
    end
  end


  def handle_response(letter, indices)
    @dictionary.select! do |word|
        word_indices = []
    word.chars.each_with_index do |ch, idx|
      word_indices << idx if ch == letter
    end
    word_indices == indices
  end
end



end
